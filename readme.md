# Exercice 1 - A small game

Let’s create a small game in HTML5. The purpose of the game is to guess a number.

Here are the specifications :

* [x] The game computes a random number between 1 and 999, the user has to guess it.
* [x] For each try, the computer will give the indication whether the guess if too high or too low.
* [x] The game ends when the user enters the right value.
* [x] The game shows a congratulation message with the number of guesses and the time taken to guess the correct number.
* [x] The UI should show a text field to enter the number and a button to submit the number (the form).
* [x] The UI should also show a list of the last 5 guesses and the result of the guess (too high or too low).
* [x] On a desktop or a tablet, the form and the list should be displayed side by side.
* [x] On a mobile, the list should be displayed above the form. The button should be bigger to be more touch friendly.

Time spent : 2h30

# How to work with source

1 install nodejs & npm
2 execute npm install grunt -g
3 execute npm install bower -g
4 npm install && bower install && grunt watch
5 Have fun !