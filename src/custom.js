/**
 * Created by Wanaham on 15/09/2015.
 */

var smallGame = function(opts){
    var options = {
        min:1,
        max:999
    };
    $.extend( options, opts );
    var number = 0;
    var status = null;
    var timeStart = new Date();
    var init = function(){
        number = Math.floor(Math.random() * options.max) + options.min;
    };
    var numberMatch = function(nb){
        return parseInt(nb) == parseInt(number);
    };
    var numberIsTooHigh = function(nb){
        return parseInt(nb) > parseInt(number);
    };
    var numberIsTooLow = function(nb){
        return parseInt(nb) < parseInt(number);
    };

    this.setNumber = function(number){
        if(numberMatch(number)){
            status = 'win';
        }else if(numberIsTooHigh(number)){
            status = 'toohigh';
        }else if(numberIsTooLow(number)){
            status = 'toolow';
        }
        return this;
    };
    this.getStatus = function(){
        return status;
    };
    this.getTimeSpent = function(){
        var diff = {};

        var tmp = new Date() - timeStart;

        tmp = Math.floor(tmp/1000);             // Nombre de secondes entre les 2 dates
        diff.sec = tmp % 60;                    // Extraction du nombre de secondes

        tmp = Math.floor((tmp-diff.sec)/60);    // Nombre de minutes (partie entière)
        diff.min = tmp % 60;                    // Extraction du nombre de minutes

        tmp = Math.floor((tmp-diff.min)/60);    // Nombre d'heures (entières)
        diff.hour = tmp % 24;                   // Extraction du nombre d'heures

        tmp = Math.floor((tmp-diff.hour)/24);   // Nombre de jours restants
        diff.day = tmp;

        return diff;
    };
    init();
};

var uiHandler = function(game, opts){
    var options = {
        message:{
            toohigh:"Too high Buddy !",
            toolow:"Too low Dude !",
            win:"Good Game ! Yeah !",
            nonumber:"Awkard ..."
        }
    };
    $.extend( options, opts );
    var formatTimeSpent = function(timer){
        var txt = [];
        if(timer.hour > 0){
            txt.push(timer.hour+' hour(s)');
        }
        if(timer.min > 0){
            txt.push(timer.min+' min(s)');
        }
        if(timer.sec > 0){
            txt.push(timer.sec+' sec(s)');
        }
        return "You guess it in "+txt.join(', ')+' !';
    };
    var submitHandler = function(){
        var win = false;
        $('form').bind({
            'submit':function(e) {
                e.preventDefault(); // this will prevent from submitting the form.
                if(win){
                    return false;
                }
                var number = $('#hitbox').val().trim();
                var toaster = $('#toaster');
                var status = game.setNumber(parseInt(number)).getStatus();
                var timeSpent = game.getTimeSpent();
                switch(status){
                    case 'win':
                        win = true;
                        toaster.removeClass('hide').addClass('animated flash green-text text-lighten-3').html(options.message.win);
                        $("<p class='center'><em>"+formatTimeSpent(timeSpent)+"<br/><a href='http://isittimeforabeer.com/'>Is it time for a beer ?</a></em></p>").insertAfter(toaster);
                        break;
                    case 'toohigh':
                        toaster.removeClass('hide').addClass('animated bounceIn red-text text-lighten-3').html(options.message.toohigh);
                        break;
                    case 'toolow':
                        toaster.removeClass('hide').addClass('animated bounceIn blue-text text-lighten-3').html(options.message.toolow);
                        break;
                    default:
                        status = 'nonumber';
                        toaster.removeClass('hide').addClass('animated bounceIn red-text text-lighten-3').html(options.message.nonumber);
                        break;
                }
                number = number.length == 0 ? '?' : number;
                $('#guess li:nth-child(5)').remove();
                $('#guess').css({'visibility':'visible'}).prepend( '<li class="collection-item"><div class="animated fadeInRight">'+number+'<small class="right">'+options.message[status]+'</small></div></li>' );
            }
        })
    };
    submitHandler();
};


$(document).ready(function(){
    var hitbox = $('#hitbox');
    var game = new smallGame({
        min:parseInt(hitbox.attr('min')),
        max:parseInt(hitbox.attr('max'))
    });
    new uiHandler(game);
});